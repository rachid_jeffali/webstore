<%--
  Created by IntelliJ IDEA.
  User: rachidj
  Date: 09/03/2021
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Article</title>
</head>
<body>
<form action="editArticle" method="post">
    Identifier: <input name="idArticle" value="${currentArticle.idArticle}" readonly="readonly"><br/>
    Brand: <input name="brand" value="${currentArticle.brand}"><br/>
    Description: <input name="description" value="${currentArticle.description}"><br/>
    Unitary Price: <input name="unitaryPrice" value="${currentArticle.unitaryPrice}" type="number"><br/>
    <br/>
    <input name="btnUpdate" type="submit" value="Update">
</form>
</body>
</html>
