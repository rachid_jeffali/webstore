<%--
  Created by IntelliJ IDEA.
  User: rachidj
  Date: 09/03/2021
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All articles</title>
</head>
<body>
    <h3> View Article - ${connectedUser.login}</h3>
    <br/>
    Identifier: ${catalogBrowser.currentArticle.idArticle}<br/>
    Brand: ${catalogBrowser.currentArticle.brand}<br/>
    Description: ${catalogBrowser.currentArticle.description}<br/>
    Unit Price: ${catalogBrowser.currentArticle.unitaryPrice}<br/>
    <br/>
    <form action="viewArticles" method="post">
        <input name="btnPrevious" type="submit" value="Previous">
        &nbsp; &nbsp;
        <input name="btnNext" type="submit" value="Next">
    </form>
    <form action="editArticle" method="get">
        <input name="btnEdit" type="submit" value="Editer">
    </form>
</body>
</html>
