<%--
  Created by IntelliJ IDEA.
  User: rachidj
  Date: 04/03/2021
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <h1>Login Screen</h1>
    <form method="post" action="login">
        Login: <input name="txtLogin" value="${login}" autofocus>
        <br/>
        Password: <input name="txtPassword"  type="password" value="${password}">
        <br/>
        <input type="submit" value="Connect">
    </form>
</body>
</html>
