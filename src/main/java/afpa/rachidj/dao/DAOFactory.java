package afpa.rachidj.dao;

import afpa.rachidj.dao.interfaces.IDAOArticle;
import afpa.rachidj.dao.interfaces.IDAOUser;
import afpa.rachidj.models.Article;
import afpa.rachidj.models.User;

public class DAOFactory {

    public static IDAOUser<User> userDAO() {
        return new UserDAO();
    }
    public static IDAOArticle<Article> articleDAO() {
        return new ArticleDAO();
    }

}
