package afpa.rachidj.dao;

import afpa.rachidj.dao.interfaces.IDAOUser;
import afpa.rachidj.models.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements IDAOUser<User> {
    @Override
    public User getUser(String login, String password) {
        String sqlQuery = "SELECT * FROM t_users WHERE login=? AND password=?";
        try(PreparedStatement statement = getConnection().prepareStatement(sqlQuery)) {
            statement.setString(1, login);
            statement.setString(2, password);
            try(ResultSet resultSet = statement.executeQuery()) {
                if(resultSet.next()){
                    return new User(
                            resultSet.getInt("idUser"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getInt("connectionNumber")
                    );
                } else {
                    return null;
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }
}
