package afpa.rachidj.dao.interfaces;

import java.util.List;

public interface IDAOArticle<T> extends IDAO<T> {
    default int getCount() { return -1; }
    default List<T> findByCriteria(String criteria) { return null;}
}
