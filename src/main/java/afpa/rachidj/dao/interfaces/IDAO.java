package afpa.rachidj.dao.interfaces;

import afpa.rachidj.dao.config.MySqlConnect;

import java.sql.Connection;
import java.util.List;

public interface IDAO<T> {
    default Connection getConnection() { return MySqlConnect.getConnection();
    }

    default boolean create(T object) {return false;}
    default T read(int id) {return null;}
    default List<T> readAll() {return null;}
    default boolean update(T object) {return false;}
    default boolean delete(T object) {return false;}

}
