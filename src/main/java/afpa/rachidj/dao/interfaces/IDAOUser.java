package afpa.rachidj.dao.interfaces;

public interface IDAOUser<T> extends IDAO<T> {
    default T getUser(String login, String password){return null;}
    default boolean isAdmin(T user) {return false;}
}
