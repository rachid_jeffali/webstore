package afpa.rachidj.dao;

import afpa.rachidj.dao.interfaces.IDAOArticle;
import afpa.rachidj.models.Article;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO implements IDAOArticle<Article> {

    @Override
    public List<Article> readAll() {
        String sqlQuery = "SELECT * FROM t_articles";
        try(Statement statement = getConnection().createStatement()) {
            List<Article> allArticles = new ArrayList<>();
            try (ResultSet resultSet = statement.executeQuery(sqlQuery)) {
                while (resultSet.next()) {
                    allArticles.add(new Article(
                            resultSet.getInt("idArticle"),
                            resultSet.getString("description"),
                            resultSet.getString("brand"),
                            resultSet.getDouble("unitaryPrice")
                    ));
                }
            }
            return allArticles;

        } catch (SQLException | NullPointerException  exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public int getCount() {
        String sqlQuery = "SELECT count(idArticle) FROM t_articles";
        try(Statement statement = getConnection().createStatement()){
            try(ResultSet resultSet = statement.executeQuery(sqlQuery)){
                resultSet.next();
                return resultSet.getInt(1);
            }
        } catch (SQLException exception){
            exception.printStackTrace();
            return -1;
        }
    }

    @Override
    public Article read(int id) {
        String sqlQuery = "SELECT * FROM t_articles WHERE idArticle=?";
        try(PreparedStatement statement = getConnection().prepareStatement(sqlQuery)){
            statement.setInt(1, id);
            try(ResultSet resultSet = statement.executeQuery()){
                resultSet.next();
                return new Article(
                        resultSet.getInt("idArticle"),
                        resultSet.getString("description"),
                        resultSet.getString("brand"),
                        resultSet.getDouble("unitaryPrice")
                );
            }
        } catch (SQLException | NullPointerException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(Article object) {

        String sqlQuery = "UPDATE t_articles SET description=?, brand=?, unitaryPrice=? WHERE idArticle=?";
        try(PreparedStatement statement = getConnection().prepareStatement(sqlQuery)){
            statement.setString(1, object.getDescription());
            statement.setString(2, object.getBrand());
            statement.setDouble(3, object.getUnitaryPrice());
            statement.setInt(4, object.getIdArticle());
            statement.executeUpdate();
            return true;
        } catch (SQLException exception){
            exception.printStackTrace();
            return false;
        }
    }
}
