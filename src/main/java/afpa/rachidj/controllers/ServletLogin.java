package afpa.rachidj.controllers;

import afpa.rachidj.dao.DAOFactory;
import afpa.rachidj.dao.UserDAO;
import afpa.rachidj.dao.interfaces.IDAOUser;
import afpa.rachidj.models.Article;
import afpa.rachidj.models.CatalogBrowser;
import afpa.rachidj.models.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "login", value = "/login")
public class ServletLogin extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("login", "");
        request.setAttribute("password", "");
        request.getRequestDispatcher("/login.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("txtLogin");
        String password = request.getParameter("txtPassword");

        request.setAttribute("login", login);
        request.setAttribute("password", password);

        User connectedUser = DAOFactory.userDAO().getUser(login, password);

        if(connectedUser != null){
            HttpSession session = request.getSession(true);
            session.setAttribute("connectedUser", connectedUser);
            session.setAttribute("catalogBrowser", new CatalogBrowser());

            request.getRequestDispatcher("/viewArticles.jsp").forward(request,response);
        } else {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }

    }
}
