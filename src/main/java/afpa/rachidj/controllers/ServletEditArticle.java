package afpa.rachidj.controllers;

import afpa.rachidj.dao.DAOFactory;
import afpa.rachidj.models.Article;
import afpa.rachidj.models.CatalogBrowser;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "editArticle", value = "/editArticle")
public class ServletEditArticle extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("connectedUser") == null){
            response.sendRedirect("login");
            return;
        }

        CatalogBrowser browser = (CatalogBrowser) session.getAttribute("catalogBrowser");

        int idCurrentArticle = browser.getCurrentArticle().getIdArticle();

        Article currentArticle = DAOFactory.articleDAO().read(idCurrentArticle);

        if (currentArticle != null){
            session.setAttribute("currentArticle", currentArticle);
            request.getRequestDispatcher("/editArticle.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/viewArticles.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
