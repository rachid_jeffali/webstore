package afpa.rachidj.controllers;

import afpa.rachidj.models.CatalogBrowser;
import afpa.rachidj.models.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "viewArticles", value = "/viewArticles")
public class ServletViewArticles extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("connectedUser") == null){
            response.sendRedirect("login");
            return;
        }
        request.getRequestDispatcher("viewArticles.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("connectedUser") == null){
            response.sendRedirect("login");
            return;
        }

        CatalogBrowser browser = (CatalogBrowser) session.getAttribute("catalogBrowser");
        if(request.getParameter("btnPrevious") != null){
            browser.goPrevious();
        } else if(request.getParameter("btnNext") != null){
            browser.goNext();
        }

        request.getRequestDispatcher("/viewArticles.jsp").forward(request, response);
    }
}
