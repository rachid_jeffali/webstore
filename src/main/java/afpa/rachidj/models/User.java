package afpa.rachidj.models;

import lombok.Data;
/**
* Object User for the authentification
 */

@Data
public class User {
    private int idUser;
    private String login;
    private String password;
    private int connectionNumber;

    public User(int idUser, String login, String password, int connectionNumber) {
        this.idUser = idUser;
        this.login = login;
        this.password = password;
        this.connectionNumber = connectionNumber;
    }

    public User() {
        this(0, "john", "doe", 0);
    }
}
