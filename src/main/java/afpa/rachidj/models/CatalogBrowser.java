package afpa.rachidj.models;

import afpa.rachidj.dao.DAOFactory;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class CatalogBrowser {

    private int currentPosition = 1;
    @Getter
    private Article currentArticle;
    private int articlesCount = DAOFactory.articleDAO().getCount();
    @Getter
    private List<ShoppingCartLine> shoppingCart = new ArrayList<>();

    public CatalogBrowser() {
        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }

    public int getShoppingCartSize() {
        int fullQuantity = 0;
        for(ShoppingCartLine line: shoppingCart) {
            fullQuantity += line.getQuantity();
        }

        return fullQuantity;
    }

    public void goPrevious() {
        if(--currentPosition < 1){
            currentPosition = articlesCount;
        }

        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }

    public void goNext() {
        if(++currentPosition > articlesCount) {
            currentPosition = 1;
        }

        currentArticle = DAOFactory.articleDAO().read(currentPosition);
    }
}
