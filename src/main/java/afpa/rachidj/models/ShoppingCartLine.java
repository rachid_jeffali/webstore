package afpa.rachidj.models;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class ShoppingCartLine {
    @Getter @Setter @NonNull
    private Article article;

    @Getter @Setter
    private int quantity;

    public ShoppingCartLine(Article article, int quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public void increaseQuantity() {
        this.quantity++;
    }
}
