package afpa.rachidj.models;

import lombok.Data;

@Data
public class Article {
    private int idArticle;
    private String description;
    private String brand;
    private double unitaryPrice;

    public Article(int idArticle, String description, String brand, double unitaryPrice) {
        this.idArticle = idArticle;
        this.description = description;
        this.brand = brand;
        this.unitaryPrice = unitaryPrice;
    }

    public Article() {
        this(0, "unknown", "unknown", 0);
    }
}
