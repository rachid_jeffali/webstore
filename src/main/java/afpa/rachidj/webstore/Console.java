package afpa.rachidj.webstore;

import afpa.rachidj.dao.DAOFactory;
import afpa.rachidj.models.Article;

import java.util.List;

public class Console {
    public static void main(String[] args) {
        List<Article> articles = DAOFactory.articleDAO().readAll();
        System.out.println(articles.size());
    }
}
